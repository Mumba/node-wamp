# Change Log

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [0.3.1] 20 Sep 2018

- Added `rethrowWampError`.

## [0.3.0] 20 Sep 2018

- Replaced observables with EventEmitter.

## [0.2.11] 1 Jun 2018

- Updated deps; removed stuff that didn't seem to belong.

## [0.2.10] 3 Apr 2018

- Updated Autobahn and other deps.

## [0.2.9] 24 Nov 2017

- Reverted export style.

## [0.2.8] 23 Nov 2017

- Updated deps.
- Fixed some faulty exports.

## [0.2.8] 12 Oct 2017

- Updated deps.
- Initialised option in WampClient construction.

## [0.2.7] 12 Oct 2017

- Added authextra to `WampClientOptions`

## [0.2.6] 6 Sep 2017

- Fixed bug resolving a cached value outside of a promise.

## [0.2.5] 5 Sep 2017

- Replaced typings with @types.

## [0.2.4] 5 Sep 2017

- Fix up declaration of Autobahn.

## [0.2.3] 5 Sep 2017

- Updated deps; added linting; fixed CI.
- Added `getAuthExtra()` to `WampClient`.
- Added caching support.

## [0.2.2] 4 Jul 2017

- Added `getOptions` to `WampClient`.

## [0.2.1] 29 May 2017

- Updated Autobahn and RxJS.

## [0.2.0] 16 Nov 2016

- Added `onError` observable. Refactored observable error handling in `register` and `subscribe`.

## [0.1.2] 19 Sep 2016

- Added `getAuthId` and `getAuthRole` to `WampClient` to expose some of the details of the caller opening the session.

## [0.1.1] 19 Sep 2016

- Added alternate initialisation path for frontend usage ([@SnareChops](https://gitlab.com/u/SnareChops)).
- Fix up tests.

## [0.1.0] 20 Jul 2016

- Initial release.
