# Mumba WAMP

Client and utilities for connecting to a Web Application Messaging Protocol ([WAMP](http://wamp-proto.org)) service.

## Installation

```sh
$ npm install --save mumba-wamp
```

## Configuration

See [Autobahn connection options](http://autobahn.ws/js/reference.html#connection-options).

## Examples

```typescript
const autobahn = require('autobahn');
import {WampClient} from `mumba-wamp`;

let options = {
	url: 'ws://localhost:8080',
	realm: 'test-realm',
	authid: '*admin',
	authmethods: ['wampcra'],
	onchallenge: (session: any, method: string, extra: any) => autobahn.auth_cra.sign('*password', extra.challenge)
};

// The main test instance.
let instance = new WampClient(options);

instance.onError.subscribe(console.error);

instance.onOpen.subscribe((session: any) => {
	console.log('Connection opened:', session);
	
	instance.closeConnection();
});

instance.onClose((details: any) => {
	console.log('Connection closed:', details);
});

instance.onSubscribe.subscribe(
	(resp: any) => console.log('Subscribed:', resp.topic, resp.id)
);

instance.onRegister.subscribe(
	(result: any) => console.log('Registered:', result.procedure, result.id)
);

instance.openConnection();
```

Alternate initialisation path if an instance of the WampClient is needed before the connection options are known.
For example a login page in a website that needs the user to enter their credentials before opening a socket connection.

```typescript
import {WampClient} from 'mumba-wamp';

...

const instance = new WampClient();

instance.setOptions(options)
	.openConnection();
...
```

Caching.

```typescript
import {WampClient} from 'mumba-wamp';
import {MemoryCache} from 'mumba-cache';

const cache = new MemoryCache({ limit: 100 });
const options = {
  url: 'ws://localhost:8080',
  realm: 'test-realm',
  authid: '*admin',
  authmethods: ['wampcra'],
  onchallenge: (session: any, method: string, extra: any) => autobahn.auth_cra.sign('*password', extra.challenge),
  
  // enable caching
  caching: {
  	enabled: true,
  	
  	// optionally whitelist the proc's to cache
  	includes: []
  }
};

const instance = new WampClient(options);

instance
  .setCache(cache)
  .openConnection();
...
```


## API

### `WampSession.get`

```js
({ authid: '*admin',
  authrole: 'backend',
  authmethod: 'wampcra',
  authprovider: 'static',
  realm: 'test-realm',
  session: 1093244371,
  transport: 
   { type: 'websocket',
     protocol: 'wamp.2.json',
     peer: 'tcp4:127.0.0.1:50979',
     http_headers_received: 
      { connection: 'Upgrade',
        upgrade: 'websocket',
        host: '127.0.0.1:8080',
        'sec-websocket-version': '13',
        'sec-websocket-key': 'MTMtMTQ1MjMxODAyNzA2Mw==',
        'sec-websocket-protocol': 'wamp.2.json',
        'sec-websocket-extensions': 'permessage-deflate; client_max_window_bits' },
     http_headers_sent: {} } })
```

## Tests

To run the test suite, first install the dependencies, then run `npm test`:

```sh
$ npm install
$ npm run docker:up
$ npm test
$ npm run docker:down
```

## References

* http://crossbar.io/docs
* http://autobahn.ws/js/reference.html

## People

The original author of _Mumba WAMP_ is [Andrew Eddie](https://gitlab.com/u/aeddie.mumba).

[List of all contributors](https://gitlab.com/Mumba/node-wamp/graphs/master)

## License

[Apache 2.0](LICENSE.txt)

* * *

&copy; 2016 [Mumba Pty Ltd](http://www.mumba.cloud). All rights reserved.

