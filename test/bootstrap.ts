/**
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import { WampClient, WampChallenge } from '../src/index';
const HOST = process.env.HOST || 'localhost';

/**
 * Create options for the WAMP connection.
 *
 * @param {Function} [challenge]
 * @returns {*}
 */
export function createOptions(challenge: any): any {
  return {
    url: 'ws://' + HOST + ':8080',
    realm: 'test-realm',
    authid: '1',
    authmethods: ['wampcra'],
    onchallenge: challenge
  };
}

/**
 * Creates a WAMP client for testing.
 */
export function createClient(): WampClient {
  const challenge = new WampChallenge();
  const options: any = createOptions(challenge.createWampCra('*password'));

  return new WampClient(options);
}
