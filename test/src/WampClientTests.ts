/**
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

const assert = require('assert');
const autobahn = require('autobahn');
import { Cache, MemoryCache } from 'mumba-cache';
import { WampClient } from '../../src/index';
import { createOptions } from '../bootstrap';

describe('WampClient functional tests', function() {
  let instance: WampClient;
  let options: any;

  beforeEach(function() {
    options = createOptions((session: any, method: string, extra: any) => {
      return autobahn.auth_cra.sign('*password', extra.challenge);
    });

    // The main test instance.
    instance = new WampClient(options);
  });

  afterEach(function() {
    instance.closeConnection();
  });

  describe('construction', () => {
    it('should throw if "url" is missing in constructor options', () => {
      delete options.url;

      assert.throws(() => {
        instance = new WampClient(options);
      }, /<options.url> required/i);
    });

    it('should throw if "realm" is missing in constructor options', () => {
      delete options.realm;

      assert.throws(() => {
        instance = new WampClient(options);
      }, /<options.realm> required/i);
    });

    it('should not throw if undefined is passed to the constructor', () => {
      options = void 0;
      assert.doesNotThrow(() => {
        instance = new WampClient();
      });
    });
  });

  describe('connections', () => {
    it('should chain opening and closing a connection', () => {
      assert.strictEqual(instance, instance.openConnection(), 'openConnection should chain');
      assert.strictEqual(instance, instance.closeConnection(), 'closeConnection should chain');
    });

    it('should allow you to subscribe to opening a connection', done => {
      instance.onOpen((session: any) => {
        try {
          assert.equal(instance.getAuthId(), '1', 'should be the authId');
          assert.equal(instance.getAuthRole(), 'backend', 'should be the authId');
          assert.strictEqual(instance.getAuthExtra(), void 0, 'should be the authextra');
          done();
        } catch (e) {
          done(e);
        }
      });

      instance.openConnection();
    });

    it('should allow you to subscribe to closing a connection', done => {
      instance.onOpen((session: any) => {
        assert(session);
        instance.closeConnection();
      });

      instance.onClose((details: any) => {
        try {
          assert.equal(details.reason, 'wamp.close.normal');
          assert.strictEqual(instance.getAuthId(), null, 'should reset the authId');
          assert.strictEqual(instance.getAuthRole(), null, 'should reset the authId');
          assert.strictEqual(instance.getAuthExtra(), null, 'should reset the authId');
          done();
        } catch (err) {
          done(err);
        }
      });

      instance.openConnection();
    });
  });

  it('should get the options', () => {
    assert.strictEqual(instance.getOptions(), options, 'should be the options');
  });

  describe('.call, without caching', () => {
    it('should call a procedure', done => {
      const subscription = instance.onOpen((session: any) => {
        instance
          .call('wamp.session.get', [session.id])
          .then((response: any) => {
            assert.equal(response.authid, options.authid, 'should get back the authid');
            done();
          })
          .catch(done);
      });

      instance.openConnection();
    });

    it('should defer calling until session is available (but only once)', done => {
      let sessionId: number;

      instance.onOpen((session: any) => {
        sessionId = session.id;
      });

      instance
        .call('wamp.registration.list')
        .then((resp: any) => {
          assert(Array.isArray(resp.exact), 'should return a dictionary');

          // Simulate a Crossbar restart.
          // TODO this is actually not tested with certainty. We need to put a spy on session.call.
          instance.on.emit('open', {
            id: sessionId
          });

          done();
        })
        .catch(done);

      instance.openConnection();
    });
  });

  describe('.call, with caching', () => {
    let cache: Cache;
    const PROC = 'wamp.registration.list';

    beforeEach(done => {
      options.cache.enabled = true;
      cache = new MemoryCache({ limit: 100 });
      instance.setCache(cache);
      instance.openConnection();
      instance.onOpen(() => done());
    });

    it('should set a value in the cache', done => {
      const KEY = JSON.stringify([PROC, null, null]);

      instance
        .call(PROC)
        .then((resp: any) => {
          assert.equal(cache.get(KEY), resp, 'should put the value into the cache');
          done();
        })
        .catch(done);
    });

    it('should use a cached value if provided', done => {
      const args: any = ['arg'];
      const kwargs = { arg: 'value' };
      const KEY = JSON.stringify([PROC, args, kwargs]);
      const expected = 'foobar';

      cache.set(KEY, expected);

      instance
        .call(PROC, args, kwargs)
        .then((resp: any) => {
          assert.equal(expected, resp);
          done();
        })
        .catch(done);
    });

    it('should not call the cache if caching disabled', () => {
      options.cache.enabled = false;
      cache.get = () => Promise.reject(new Error('should not call'));

      return instance.call(PROC);
    });

    it('should call the cache if proc is white listed', () => {
      const KEY = JSON.stringify([PROC, null, null]);
      const expected = 'foobar2';

      cache.set(KEY, expected);
      options.cache.includes = [PROC];

      return instance.call(PROC).then((result: number) => {
        assert.equal(result, expected, 'should be the number of sessions');
      });
    });

    it('should not call the cache if proc is not white listed', () => {
      options.cache.includes = ['foo'];
      cache.get = () => Promise.reject(new Error('should not call'));

      return instance.call('wamp.session.count').then((result: number) => {
        // console.log(result);
        assert(result > 0, 'should be the number of sessions');
      });
    });
  });

  describe('.register', () => {
    it('should be able to register a procedure', done => {
      function testProcedure() {
        done();
      }

      instance.onOpen((session: any) => {
        instance.register('test.procedure', testProcedure);

        instance.onRegister((response: any) => {
          try {
            assert.equal(response.procedure, 'test.procedure');

            // Call the proc and finish the test.
            instance.call('test.procedure').catch(done);
          } catch (e) {
            done(e);
          }
        });
      });

      instance.openConnection();
    });

    it('should defer registering until session is available', done => {
      instance.register('defer-register', () => {
        done();
      });

      instance.onRegister((response: any) => {
        instance.call('defer-register').catch(done);
      });

      instance.openConnection();
    });

    it('should catch an error while registering a procedure', done => {
      function testProcedure() {}

      instance.onError((err: any) => {
        try {
          assert.equal(err.error, 'wamp.error.procedure_already_exists', 'should catch the error');
          done();
        } catch (e) {
          done(e);
        }
      });

      instance.onOpen((session: any) => {
        instance.register('test.procedure', testProcedure);
        instance.register('test.procedure', testProcedure);
      });

      instance.openConnection();
    });
  });

  describe('.subscribe', () => {
    it('should be able to subscribe to a topic', done => {
      instance.onOpen(() => {
        assert.strictEqual(instance, instance.subscribe('wamp.session.on_join', () => {}), 'should chain');

        instance.onSubscribe((resp: any) => {
          try {
            assert.equal(resp.topic, 'wamp.session.on_join');
            done();
          } catch (e) {
            done(e);
          }
        });
      });

      instance.openConnection();
    });

    it('should defer subscribing until session is available', done => {
      instance.subscribe('defer-subscribe', () => {
        done();
      });

      instance.onSubscribe((response: any) => {
        assert.equal(response.topic, 'defer-subscribe');

        return instance.publish(
          'defer-subscribe',
          ['deferred'],
          {},
          {
            exclude_me: false
          }
        );
      });

      instance.openConnection();
    });

    it('should catch an error when subscribing', done => {
      instance.onError((resp: any) => {
        try {
          assert.equal(resp.error, 'wamp.error.not_authorized', 'should catch the error');
          done();
        } catch (e) {
          done(e);
        }
      });

      instance.onOpen(() => {
        instance.subscribe('nosub.foo', () => {});
      });

      instance.openConnection();
    });
  });

  describe('.publish', () => {
    it('should be able to publish to a topic when the session already exists', done => {
      instance.onOpen((session: any) => {
        instance
          .publish('test.topic', [], {}, { acknowledge: true })
          .then((response: any) => {
            assert(response.id);
            done();
          })
          .catch(done);
      });

      instance.openConnection();
    });

    it('should defer publishing until session is available (but only once)', done => {
      let sessionId: number;

      instance.onOpen((session: any) => {
        sessionId = session.id;
      });

      instance
        .publish(
          'defer-publish',
          ['deferred the publish'],
          {},
          {
            acknowledge: true,
            exclude_me: false
          }
        )
        .then(() => {
          // Simulate a Crossbar restart.
          // TODO this is actually not tested with certainty. We need to put a spy on session.publish.
          instance.on.emit('open', {
            id: sessionId
          });

          done();
        })
        .catch(done);

      instance.openConnection();
    });
  });
});
