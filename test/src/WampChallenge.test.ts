/**
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import * as assert from 'assert';
import { WampChallenge } from '../../src';

describe('WampChallenge unit tests', () => {
  it('should create a WampCRA challenge', () => {
    const wampChallenge = new WampChallenge();
    const challenge = wampChallenge.createWampCra('secret');

    assert(challenge(123, 'unknown', {}) === '', 'if not wampcra, should return an empy string');
    assert(challenge(123, 'wampcra', {}).length > 1, 'should return a string of some length');
  });
});
