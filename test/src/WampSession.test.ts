/**
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import * as assert from 'assert';
import { WampSession, WampSessionEvents, WampClient } from '../../src/index';
import { createClient } from '../bootstrap';

describe('WampSessionEvents unit tests', () => {
  it('should include a dictionary of the meta events', () => {
    assert.equal(WampSessionEvents.ON_JOIN, 'wamp.session.on_join');
    assert.equal(WampSessionEvents.ON_LEAVE, 'wamp.session.on_leave');
  });
});

describe('WampSession functional tests', function() {
  let client: WampClient;
  let instance: WampSession;
  let sessionId: number;

  beforeEach(function(done) {
    client = createClient();
    client.openConnection();
    client.onOpen((session: any) => {
      sessionId = session.id;
      done();
    });
    instance = new WampSession(client);
  });

  afterEach(function() {
    client.closeConnection();
  });

  it('should count the sessions', () => {
    return instance.count().then((resp: number) => {
      assert(resp > 0, 'should be at least 1 connection');
    });
  });

  it('should list session IDs attached to the realm', () => {
    return instance.list().then((resp: number[]) => {
      assert(resp.length > 0, 'should be at least 1 connection');
    });
  });

  it('should get data about a specific session', () => {
    return instance.get(sessionId).then((resp: any) => {
      assert.equal(resp.authid, '1', 'should get the meta information');
    });
  });

  it('should get data no data about an unknown session', () => {
    return instance.get(null).then((resp: any) => {
      assert.deepEqual(resp, {}, 'should be an empty object');
    });
  });
});
