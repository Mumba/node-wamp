const autobahn = require('autobahn');
const config = require('./config');
const WampClient = require('../dist/src/index').WampClient;

let options = {
  ...config,
  onchallenge: (session, method, extra) => autobahn.auth_cra.sign(config.password, extra.challenge)
};

// The main test instance.
let instance = new WampClient(options);
console.log(instance.on)

instance.on.addListener('error', console.error);

instance.on.addListener('open', (session) => {
  console.log('Connection opened:', session);

  instance.closeConnection();
});

instance.on.addListener('close', (details) => {
  console.log('Connection closed:', details);
});

instance.openConnection();