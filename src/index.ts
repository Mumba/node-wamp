/**
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

export { WampClient, WampClientOptions } from './WampClient';
export { WampSession, WampSessionEvents } from './WampSession';
export { WampChallenge } from './WampChallenge';

function isWampError(error: any) {
  // TODO might need more comprehensive checks in future.
  return Array.isArray(error.args);
}

/**
 * Rethrows an error if it's an Autobahn error format.
 */
export function rethrowWampError(e: any) {
  if (isWampError(e)) {
    // TODO may need to join the args in the future.
    throw new Error(e.args[0]);
  }

  throw e;
}
