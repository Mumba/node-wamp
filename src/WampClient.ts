/**
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import * as autobahn from 'autobahn';
import EventEmitter = require('eventemitter3');
import { ListenerFn } from 'eventemitter3';
import { Cache, NullCache } from 'mumba-cache';
import IConnectionOptions = autobahn.IConnectionOptions;

export interface WampClientOptions extends IConnectionOptions {
  cache?: {
    enabled?: boolean;
    includes?: string[];
  };
  authextra?: any;
}

/**
 * WAMP Client
 */
export class WampClient {
  public on = new EventEmitter<any>();

  private cache = new NullCache();
  private connection: any;
  private options: any = {};
  private session: any;
  private authId: string;
  private authRole: string;
  private authExtra: any;

  /**
   * WAMP client Constructor
   */
  constructor(options?: any) {
    if (!!options) {
      this.setOptions(options);
    }
  }

  /**
   * Set the options for the connection.
   */
  public setOptions(options: WampClientOptions = {}): this {
    this.validationOptions(options);
    this.options = options || {};

    if (!this.options.cache) {
      this.options.cache = {
        enabled: false
      };
    }

    return this;
  }

  public getOptions(): any {
    return this.options;
  }

  /**
   * Get the authid of the caller who opened the current session.
   */
  public getAuthId(): string {
    return this.authId;
  }

  /**
   * Get the authrole of the caller who opened the current session.
   */
  public getAuthRole(): string {
    return this.authRole;
  }

  /**
   * Get the authextra of the caller who opened the current session.
   */
  public getAuthExtra(): string {
    return this.authExtra;
  }

  public setCache(cache: Cache) {
    this.cache = cache;
  }

  public onOpen(f: ListenerFn) {
    this.on.addListener('open', f);
  }

  public onClose(f: ListenerFn) {
    this.on.addListener('close', f);
  }

  public onRegister(f: ListenerFn) {
    this.on.addListener('register', f);
  }

  public onSubscribe(f: ListenerFn) {
    this.on.addListener('subscribe', f);
  }

  public onError(f: ListenerFn) {
    this.on.addListener('error', f);
  }

  /**
   * Open the connection with the WAMP server.
   */
  public openConnection(): this {
    this.createConnection();
    this.connection.open();

    return this;
  }

  /**
   * Close the connection with the WAMP server.
   */
  public closeConnection(): this {
    if (this.connection && this.connection.isConnected) {
      this.connection.close();
    }

    this.session = null;

    return this;
  }

  /**
   * Call a remote procedure from a session.
   *
   * @param {string} procedure                  - the URI of the procedure to call.
   * @param {array}  [args]                     - call arguments (array form).
   * @param {object} [kwargs]                   - call arguments (dictionary form).
   * @param {object} [options]
   * @param {object} [options.receive_progress] - allows a remote procedure can return progressive results. Default: false.
   * @param {boolean} [options.disclose_me]     - discloses the identity of the caller. Default: false.
   * @returns {Promise}
   * @see {@link http://autobahn.ws/js/reference.html#call}
   */
  public call(procedure: string, args?: any[], kwargs?: any, options?: any) {
    if (this.hasSession()) {
      return this._call(procedure, args, kwargs, options);
    }

    return new Promise(resolve => {
      this.on.once('open', () => {
        resolve(this._call(procedure, args, kwargs, options));
      });
    });
  }

  /**
   * Register a procedure on a session.
   *
   * @param {string}   procedure        - the URI of the procedure to register
   * @param {callable} endpoint         - the function that provides the procedure implementation
   * @param {object}   [options]
   * @param {string}   [options.match]  - `prefix`, `wildcard`. Default: nothing (exact matching).
   * @param {string}   [options.invoke] - `first`, `last`, `roundrobin`, `random`, `single`. Default: `single`.
   * @returns {Promise}
   * @see {@link http://autobahn.ws/js/reference.html#register}
   */
  public register(procedure: string, endpoint: Function, options?: any) {
    const register = () => {
      this.session
        .register(procedure, endpoint, options)
        .then((resp: any) => this.on.emit('register', resp))
        .catch((err: Error) => this.on.emit('error', err));
    };

    this.on.addListener('open', () => {
      register();
    });

    if (this.hasSession()) {
      register();
    }

    return this;
  }

  /**
   * Subscribe to a topic on a session.
   *
   * @param {string}   topic            - the URI of the topic to subscribe to.
   * @param {callable} handler          - the event handler that should consume events.
   * @param {object}   [options]
   * @param {string}   [options.match]  - `prefix`, `wildcard`. Default: nothing (exact matching).
   * @returns {WampClient}
   * @see {@link http://autobahn.ws/js/reference.html#subscribe}
   */
  public subscribe(topic: string, handler: Function, options?: any): this {
    const subscribe = () => {
      this.session
        .subscribe(topic, handler, options)
        .then((resp: any) => this.on.emit('subscribe', resp))
        .catch((err: Error) => this.on.emit('error', err));
    };

    this.on.addListener('open', () => {
      subscribe();
    });

    if (this.hasSession()) {
      subscribe();
    }

    return this;
  }

  /**
   * Publishes an event on a session.
   *
   * @param {string}  topic                 - the URI of the topic to publish to
   * @param {array}   [args]                - application event payload
   * @param {object}  [kwargs]              - application event payload
   * @param {object}  [options]             - specifies options for publication
   * @param {boolean} [options.acknowledge] - if true, publish will return a Promise (otherwise nothing)
   * @param {array}   [options.exclude]     - an array of WAMP session IDs that WILL NOT receive a published event
   * @param {array}   [options.eligible]    - an array of WAMP session IDs that WILL receive a published event
   * @param {boolean} [options.exclude_me]  - allow for the publisher to receive the event. Default: true.
   * @param {boolean} [options.disclose_me] - discloses the identity of the publisher. Default: false.
   * @returns {Promise|void}
   * @see {@link http://autobahn.ws/js/reference.html#publish}
   */
  public publish(topic: string, args?: any[], kwargs?: any, options?: any): Promise<any> {
    if (this.hasSession()) {
      return this.session.publish(topic, args, kwargs, options);
    } else {
      return new Promise(resolve => {
        this.on.once('open', () => {
          resolve(this.session.publish(topic, args, kwargs, options));
        });
      });
    }
  }

  /**
   * Validation the options passed to the constructor.
   */
  private validationOptions(options: any = {}) {
    if (!options.url) {
      throw new Error('WampClient: <options.url> required');
    }

    if (!options.realm) {
      throw new Error('WampClient: <options.realm> required');
    }

    return this;
  }

  /**
   * Creates the connection with the WAMP server.
   */
  private createConnection() {
    this.connection = new autobahn.Connection(this.options);

    this.connection.onopen = (session: any, details: any) => {
      this.session = session;
      this.authId = details.authid;
      this.authRole = details.authrole;
      this.authExtra = details.authextra;
      this.on.emit('open', session);
    };

    this.connection.onclose = (reason: string, details: any) => {
      this.session = null;
      this.authId = null;
      this.authRole = null;
      this.authExtra = null;
      this.on.emit('close', details);
    };
  }

  /**
   * Checks if this object has a session registered.
   */
  private hasSession(): boolean {
    return !!this.session;
  }

  private isCaching(procedure: string) {
    if (!this.options.cache.enabled) {
      return false;
    }

    if (Array.isArray(this.options.cache.includes)) {
      return this.options.cache.includes.indexOf(procedure) > -1;
    }

    return this.options.cache.enabled;
  }

  private _call(procedure: string, args: any[], kwargs: any, options: any) {
    return this.isCaching(procedure)
      ? this._callWithCache(procedure, args, kwargs, options)
      : this._callNoCache(procedure, args, kwargs, options);
  }

  private _callNoCache(procedure: string, args: any[], kwargs: any, options: any) {
    return this.session.call(procedure, args, kwargs, options);
  }

  private _callWithCache(procedure: string, args: any[], kwargs: any, options: any) {
    return new Promise((resolve, reject) => {
      const key = JSON.stringify([procedure, args, kwargs]);
      const value = this.cache.get(key);

      if (value) {
        return resolve(value);
      }

      this._callNoCache(procedure, args, kwargs, options)
        .then((result: any) => {
          resolve(this.cache.set(key, result));
        })
        .catch(reject);
    });
  }
}
