/**
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import autobahn = require('autobahn');

/**
 * Factory to create WAMP challenge handling functions.
 */
export class WampChallenge {
  /**
   * Returns a WampCRA challenge function.
   *
   * @param {string} secret
   * @returns {function(any, string, any=): (string|string)}
   */
  public createWampCra(secret = '') {
    return (session: any, method: string, extra: any = {}) => {
      if (method !== 'wampcra') {
        return '';
      }

      return autobahn.auth_cra.sign(secret, extra.challenge);
    };
  }
}
