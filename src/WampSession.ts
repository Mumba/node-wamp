/**
 * WAMP router session meta API.
 *
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import { WampClient } from './WampClient';
/**
 * WAMP router session events
 */
export class WampSessionEvents {
  /**
   * Triggered when a session joins a realm on the router.
   *
   * @type {string}
   */
  public static ON_JOIN = 'wamp.session.on_join';

  /**
   * Triggered when a session leaves a realm on the router or is disconnected.
   *
   * @type {string}
   */
  public static ON_LEAVE = 'wamp.session.on_leave';
}

/**
 * API for WAMP session meta events and procedures.
 */
export class WampSession {
  /**
   * Constructor.
   *
   * @param {WampClient} wampClient
   */
  constructor(private wampClient: WampClient) {}

  /**
   * Returns the number of sessions currently attached to the realm.
   *
   * @returns {Promise<number>}
   */
  public count(): Promise<number> {
    return this.wampClient.call('wamp.session.count');
  }

  /**
   * Returns an array of the session IDs for all sessions currently attached to the realm.
   *
   * @returns {Promise<number[]>}
   */
  public list(): Promise<number[]> {
    return this.wampClient.call('wamp.session.list');
  }

  /**
   * Returns data about the session itself:
   * the realm, authentication provider, ID, role and method, as well as about the transport used.
   * @param {object} sessionId
   * @returns {Promise<any>}
   */
  public get(sessionId: number): Promise<any> {
    if (!sessionId) {
      return Promise.resolve({});
    }

    return this.wampClient.call('wamp.session.get', [sessionId]);
  }
}
